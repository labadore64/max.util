﻿using System;
namespace max.util
{
    public static class PlatformUtil
    {
        /// <summary>
        /// The different platform types.
        /// </summary>
        public enum PlatformType
        {
            /// <summary>
            /// Windows
            /// </summary>
            WINDOWS,
            /// <summary>
            /// Unix/Linux
            /// </summary>
            UNIX,
            /// <summary>
            /// Invalid
            /// </summary>
            INVALID
        }

        /// <summary>
        /// Gets a string correlating with the OS running.
        /// </summary>
        /// <returns>"windows", "unix" or "invalid".</returns>
       public static PlatformType Platform
        {
            get
            {
                OperatingSystem os = Environment.OSVersion;
                PlatformID pid = os.Platform;
                switch (pid)
                {
                    case PlatformID.Win32NT:
                    case PlatformID.Win32S:
                    case PlatformID.Win32Windows:
                    case PlatformID.WinCE:
                        return PlatformType.WINDOWS;
                    case PlatformID.Unix:
                        return PlatformType.UNIX;
                    default:
                        return PlatformType.INVALID;
                }
            }
        }
    }
}
