﻿using System;

namespace max.util
{
    /// <summary>
    /// A class that performs common string manipulations.
    /// </summary>
    public static class StringUtil
    {
        /// <summary>
        /// Represents a variable to be replaced. All variables are followed by a number.
        /// </summary>
        /// <value>Prefix</value>
        public static string ReplacePrefix { get; set; } = "0x&";

        /// <summary>
        /// Trims a string and removes trailing slashes and periods.
        /// </summary>
        /// <param name="String">String</param>
        /// <returns>Modified string</returns>
        public static string StripString(string String)
        {
            String = String.Trim();
            //location must not start or end with forward or backslash
            char[] test = String.ToCharArray();

            if (test[0] == '\\'
                || test[0] == '/'
                || test[0] == '.')
            {
                String = String.Remove(0, 1);
            }

            int i = test.Length - 1;

            if (test[i] == '\\'
                || test[i] == '/'
                || test[i] == '.')
            {
                String = String.Remove(i, 1);
            }

            return String;
        }

        /// <summary>
        /// Trims whitespace from a string, and trims the string to a certain length.
        /// </summary>
        /// <param name="String">String</param>
        /// <param name="Size">Max length of the generated string</param>
        /// <returns>Modified string</returns>
        public static string ClipString(string String, int Size)
        {
            if (Empty(String))
            {
                return "";
            }
            if(String.Length < Size)
            {
                return String;
            }
            return String.Trim().Substring(0, Size);
        }

        /// <summary>
        /// Checks if a string is empty.
        /// </summary>
        /// <param name="String">String</param>
        /// <returns>True/False</returns>
        public static bool Empty(string String)
        {
            return String == null || String.Trim().Length == 0;
        }

        /// <summary>
        /// Replaces all instances of ReplacePrefix[n] with the variables in the array.
        /// </summary>
        /// <param name="String">String</param>
        /// <param name="Variables">Variables to insert</param>
        /// <returns>Modified string</returns>
        public static string ReplaceVariables(string String, string[] Variables)
        {
            for(int i = 0; i < Variables.Length; i++)
            {
                String = String.Replace(ReplacePrefix + (i + 1), Variables[i]);
            }

            return String;
        }
    }
}
