﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace max.util
{
    /// <summary>
    /// A class that contains methods for reflection.
    /// </summary>
    public static class ReflectionUtil
    {
        /// <summary>
        /// Gets the type from a full type string.
        /// </summary>
        /// <param name="TypeID">Type string</param>
        /// <returns>Type</returns>
        public static Type FullStringToType(string TypeID)
        {
            return Type.GetType(TypeID);
        }

        /// <summary>
        /// Gets the type from a type string.
        /// </summary>
        /// <param name="TypeID">Type string</param>
        /// <returns>Type</returns>
        public static Type StringToType(string TypeID)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => p.AssemblyQualifiedName.Contains(TypeID)).First();
        }

        /// <summary>
        /// Gets the method from a string.
        /// </summary>
        /// <param name="MethodID">Method's ID string</param>
        /// <returns>Method</returns>
        public static MethodInfo FullStringToMethod(string MethodID)
        {
            // gets the index to split on
            int index = MethodID.LastIndexOf(":");

            string type;

            if (index > 0)
            {
                // split the string in two - the left side is the type, the right side is the Method
                type = MethodID.Substring(0, index); 
                MethodID = MethodID.Substring(index + 1, MethodID.Length - index - 1);
            } else
            {
                // if the split fails throw an exception
                throw new ArgumentException("MethodID must be be delimited by \":\". The left should contain the" +
                    " method's type and assembly, the right should be the name of the method.");
            }

            try
            {
                // Get the type of a specified class.
                return Type.GetType(type).GetMethod(MethodID);
            }
            catch (TypeLoadException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the method from a string.
        /// </summary>
        /// <param name="MethodID">Method's ID string</param>
        /// <returns>Method</returns>
        public static MethodInfo StringToMethod(string MethodID)
        {
            // gets the index to split on
            int index = MethodID.LastIndexOf(":");

            string type;

            if (index > 0)
            {
                // split the string in two - the left side is the type, the right side is the Method
                type = MethodID.Substring(0, index);
                MethodID = MethodID.Substring(index + 1, MethodID.Length - index - 1);
            }
            else
            {
                // if the split fails throw an exception
                throw new ArgumentException("MethodID must be be delimited by \":\". The left should contain the" +
                    " method's type and assembly, the right should be the name of the method.");
            }

            try
            {
                // Get the type of a specified class.
                return StringToType(type).GetMethod(MethodID);
            }
            catch (TypeLoadException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        /// <summary>
        /// Returns the full name of the type and the method defined by the passed Delegate, delimited by ":".
        /// </summary>
        /// <param name="Delegate">Method</param>
        /// <returns>Full name and type in string</returns>
        public static string DelegateToString(Delegate Delegate)
        {
            return FullTypeToString(Delegate.Method.ReflectedType) + ":" + Delegate.Method.Name;
        }

        /// <summary>
        /// Returns the full name of the type and its assembly, delimited by ", ".
        /// </summary>
        /// <param name="Type">Type</param>
        /// <returns>Full name and assembly name in string</returns>
        public static string FullTypeToString(Type Type)
        {
            return Type.FullName + ", " + Type.Assembly.GetName().Name;
        }


        /// <summary>
        /// Returns the name of the type and its assembly, delimited by ", ".
        /// </summary>
        /// <param name="Type">Type</param>
        /// <returns>Full name and assembly name in string</returns>
        public static string TypeToString(Type Type)
        {
            return Type.Name + ", " + Type.Assembly.GetName().Name;
        }

        /// <summary>
        /// Returns the method and its type and assembly, delimited by ".".
        /// </summary>
        /// <param name="Type">Type</param>
        /// <returns>Full name and assembly name in string</returns>
        public static string FullMethodToString(MethodInfo Method)
        {
            return FullTypeToString(Method.ReflectedType) + "." + Method.Name;
        }

        /// <summary>
        /// Returns all types in the current loaded assemblies that inherit the passed type.
        /// </summary>
        /// <param name="Type">Type to check</param>
        /// <returns>List of types found</returns>
        public static List<Type> GetTypesContainingType(Type Type)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => Type.IsAssignableFrom(p)).ToList();
        }
    }
}

