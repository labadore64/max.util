# Using max.util

``max.util`` is a library that has a set of utilities for a wide variety of generic tasks.

* [Installation](Installation.md)
* Utilities:
	* [Strings](Strings.md)
	* [Reflection](Reflection.md)
	* [Other](Other.md)

## Max Dependencies:

``max.util`` does not depend on any other Max libraries.