# String Utilities

The ``StringUtil`` class contains several methods to help you perform string manipulations.

## Variable Replacing

Replacing variables in text strings is easy.

The prefix string is set by the variable ``ReplacePrefix`` and defaults to ``0x&``. Each variable in the text will reference a string in an array.

For example, here is a sample string with 3 variables:

```
string Ponies = 
	"Hello, 0x&1! I like ponies! I have 0x&2 ponies at home. My favorite color pony are 0x&3 ponies."
```

Here is a sample of replacing this string's variables:

```
string PoniesResult = 
	StringUtil.ReplaceVariables(
		Ponies,
		new string[] {
			"John",
			"33",
			"green"
	});

Console.WriteLine(PoniesResult);
```

And here is the output from printing that result:

```
Hello, John! I like ponies! I have 33 ponies at home. My favorite color pony are green ponies.
```

## Other Utilities

### Clip String

To trim a string to a set number of characters, you can use ``ClipString``. This method also removes all whitespace characters surrounding the string.

```
string HorseName = StringUtil.ClipString("           A Really Long Horse name",12);
Console.WriteLine(HorseName); // prints "A Really Lo"
```

### Check if String is Empty

Checks if a string is null or empty.

```
if(StringUtil.Empty("hello")){
	// this will never execute
}

if(StringUtil.Empty("")){
	// this will always execute
}

if(StringUtil.Empty(null)){
	// this will always execute
}
```