# Other Utilities

## Platform Utility

The ``PlatformUtil`` class contains methods dealing with the current running platform.

### Current Platform

You can get whether the current running platform is Windows, Unix or something else.

```
PlatformUtil.PlatformType Type = PlatformUtil.Platform();
if(Type == PlatformUtil.PlatformType.WINDOWS){
	Console.WriteLine("Bear");
} else if (Type == PlatformUtil.PlatformType.UNIX){
	Console.WriteLine("Penguin");
}
```