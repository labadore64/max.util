# Reflection

The ``ReflectionUtil`` class contains several methods to help with reflection.

## Reflection and Strings

For serialization purposes, it might be useful to transform ``Delegate`` and ``Type`` into strings, and return them back:

```
string Method = ReflectionUtil.DelegateToString(ReflectionDelegate);
string Type = ReflectionUtil.TypeToString(ReflectionDelegate.GetType());

// This prints out:
// max.util.demo.ReflectionDemo.TestMethod, max.util:TestMethod

Console.WriteLine(Method);

// This prints out:
// max.util.demo.ReflectionDemo.TestMethod, max.util

Console.WriteLine(Type);
```

The type string will contain the full name of the type along with the assembly name.

You can also get a ``Type`` and ``MethodInfo`` from strings:

```
Type type = ReflectionUtil.StringToType(Type);

MethodInfo method = ReflectionUtil.StringToMethod(Method);

// you can even make a delegate after you're done!

DemoDelegate del = (DemoDelegate)method.CreateDelegate(typeof(DemoDelegate));

```


## Other Utilities

### Get all types that implement/inherit another type

You can get a list of all types that inherit or implement a certain type:

```
List<Type> Types = ReflectionUtil.GetTypesContainingType(typeof(Bird));
foreach(Type t in Types){
	Console.WriteLine(t.Name);
}
```