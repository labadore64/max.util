# max.util

``max.util`` is a MaxLib Helper Library that offers various C# utilities, such as string manipulation and reflection.

* [Documentation](https://labadore64.gitlab.io/max.util/)
* [Source](https://gitlab.com/labadore64/max.util/)
